document.getElementById("hello_text").textContent="初めてのJavaScript";

console.log('main.js')
speed=1000 // msec
limit=500 // sec

var count=0;
var cells;
var blocks={
  i:{
    class:"i",
    pattern:[
      [1,1,1,1]
    ]
  },
  t:{
    class:"t",
    pattern:[
      [0,1,0],
      [1,1,1]
    ]
  },
  o:{
    class:"o",
    pattern:[
      [1,1],
      [1,1]
    ]
  },
  s:{
    class:"s",
    pattern:[
      [0,1,1],
      [1,1,0]
    ]
  },
  z:{
    class:"z",
    pattern:[
      [1,1,0],
      [0,1,1]
    ]
  },
  j:{
    class:"j",
    pattern:[
      [1,0,0],
      [1,1,1]
    ]
  },
  l:{
    class:"l",
    pattern:[
      [0,0,1],
      [1,1,1]
    ]
  },
}
loadTable();
var id=setInterval(function(){
  count++;
  document.getElementById("hello_text").textContent="初めてのJavaScript("+count+")";
  if (hasFallingBlocks()){
    fallBlocks();
  } else {
    deleteRow();
    if(isGameOver() || count>limit*speed){
      alert("Game Over")
      clearInterval(id);
    }
    generateBlock();
  }
},speed)

function loadTable(){
  var td_array=document.getElementsByTagName("td");
  cells=[];
  var index=0;
  for (var row=0;row<20; row++){
    cells[row]=[];
    for (var col=0;col<10; col++){
      cells[row][col]=td_array[index];
      index++;
    }
  }
}

function fallBlocks(){
  for (var col=0;col<10;col++){
    if (cells[19][col].blockNum===fallingBlockNum){
      isFalling=false;
      return;
    }
  }
  for (var row=18;row>=0;row--){
    for (var col=0;col<10;col++){
      if(cells[row][col].blockNum===fallingBlockNum){
        if(cells[row+1][col].className!==""&&cells[row+1][col].blockNum!==fallingBlockNum){
          isFalling=false
          return
        }
      }
    }
  }
  for (var row=18;row>=0;row--){
    for (var col=0;col<10;col++){
      if(cells[row][col].blockNum===fallingBlockNum){
        cells[row+1][col].className=cells[row][col].className;
        cells[row+1][col].blockNum=cells[row][col].blockNum;
        cells[row][col].className=""
        cells[row][col].blockNum=null;
      }
    }
  }
}

var isFalling=false;
function hasFallingBlocks(){
  for (var col=0;col<10;col++){
    return isFalling
  }
}

function deleteRow(){
  for (var row=19;row>=0;row--){
    var canDelete=true;
    for (var col=0;col<10;col++){
      if(cells[row][col].className===""){
        canDelete=false;
        break;
      }
    }
    if (canDelete){
      for (var col=0;col<10;col++){
        console.log(row,col)
        cells[row][col].className="";
        cells[row][col].blockNum=null;
      }
      for (var downRow=row-1;downRow>=0;downRow--){
        for (var col=0;col<10;col++){
          console.log(downRow+1,col)
          cells[downRow+1][col].className=cells[downRow][col].className;
          cells[downRow+1][col].blockNum=cells[downRow][col].blockNum;
          cells[downRow][col].className="";
          cells[downRow][col].blockNum=null;
        }
      }
      row++
    }
  }
}

var fallingBlockNum=0;
function generateBlock(){
  var keys=Object.keys(blocks);
  var nextBlockKey=keys[Math.floor(Math.random()*keys.length)];
  var nextBlock=blocks[nextBlockKey];
  var nextFallingBlockNum=fallingBlockNum+1
  console.log(nextBlock.class)

  var pattern=nextBlock.pattern;
  for (var row=0;row<pattern.length;row++){
    for (var col=0;col<pattern[row].length;col++){
      if(pattern[row][col]){
        cells[row][col+3].className=nextBlock.class;
        cells[row][col+3].blockNum=nextFallingBlockNum;
      }
    }
  }
  isFalling=true;
  fallingBlockNum=nextFallingBlockNum;
}

document.addEventListener("keydown",onKeyDown)
function onKeyDown(event){
  if(event.keyCode===37){
    moveRightLeft(-1);
  } else if(event.keyCode===39) {
    moveRightLeft(1);
  } else if(event.keyCode===40) {
    fallBlocks();
  }
}

function moveRightLeft(sgn){
  for (row=0;row<20;row++){
    if(cells[row][9*((1+sgn)/2)].blockNum===fallingBlockNum){
      return;
    }
  }
  for (var row=0;row<20;row++){
    for (var col=1+7*((sgn+1)/2);sgn*col>=9*((sgn-1)/2);col=col-sgn){
      if(cells[row][col].blockNum===fallingBlockNum && cells[row][col+sgn].blockNum!==fallingBlockNum && cells[row][col+sgn].blockNum!==null && cells[row][col+sgn].blockNum!==undefined){
        console.log(cells[row][col].blockNum,cells[row][col+sgn].blockNum,fallingBlockNum)
        return;
      }
    }
  }
  for (var row=0;row<20;row++){
    for (var col=1+7*((sgn+1)/2);sgn*col>=9*((sgn-1)/2);col=col-sgn){
      if(cells[row][col].blockNum===fallingBlockNum){
        cells[row][col+sgn].className=cells[row][col].className;
        cells[row][col+sgn].blockNum=cells[row][col].blockNum;
        cells[row][col].className="";
        cells[row][col].blockNum=null;
      }
    }
  }
}

function isGameOver(){
  for (var row=0;row<2;row++){
    for(var col=3;col<7;col++){
      if(cells[row][col].className!==""){
        console.log(row,col,cells[row][col].className)
        return true;
      }
    }
  }
  return false
}
